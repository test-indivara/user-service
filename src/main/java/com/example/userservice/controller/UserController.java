package com.example.userservice.controller;

import com.example.userservice.model.User;
import com.example.userservice.model.UserModel;
import com.example.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping()
    UserModel findUserByUsername(@RequestParam String username){
        return userService.findUserByUsername(username);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    UserModel saveUser(@RequestBody UserModel um){
        return userService.saveUser(um);
    }
}

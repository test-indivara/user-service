package com.example.userservice.service;

import com.example.userservice.model.User;
import com.example.userservice.model.UserModel;
import com.example.userservice.model.UserProduct;
import com.example.userservice.repository.UserProductRepository;
import com.example.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserProductRepository userProductRepository;

    public UserModel saveUser(UserModel userModel){
        if(userModel.getUserProducts().size() > 0){
            userRepository.save(userModel.getUser());
            userProductRepository.saveAll(userModel.getUserProducts());
            return userModel;
        }
        return new UserModel();
    }

    public UserModel findUserByUsername(String username){
        UserModel userModel = new UserModel();
        User user = userRepository.findById(username).orElse(null);
        if(user != null){
            userModel.setUser(user);
            List<UserProduct> userProducts = userProductRepository.findByUsername(username);
            if(userProducts.size() > 0){
                userModel.setUserProducts(userProducts);
            }
        }
        return  userModel;
    }
}
